import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main

{
    public static String diagTest;

    public static String scheduledTest;

    public static String scannerResult;

    public static void main(String[] args) throws IOException, JSONException, InterruptedException {

        diagTest = "d1";

        scheduledTest = "3";

        try {

            File gatewayID = new File("C:\\Users\\Public\\IdeaProjects\\GatewayCommunication\\src\\gatewayIDFile");

            if (!gatewayID.exists()) {
                gatewayID.createNewFile();
            }



            StringBuilder idBuilder = new StringBuilder();

            BufferedReader br = new BufferedReader(new FileReader(gatewayID));

            String st;
            while ((st = br.readLine()) != null){
                idBuilder.append(st);
            }

            String data = idBuilder.toString();

            boolean hasDigit = data.matches(".*\\d+.*");


            if (hasDigit == true) {
                System.out.println("ID already assigned.");
                scannerResult = data;
                System.out.println("Gateway ID is: " + scannerResult);
            }

            else {

                System.out.println("Please input gateway ID: ");
                Scanner reader = new Scanner(System.in);
                scannerResult = reader.nextLine();
                //WRITE TO FILE HERE

                Files.write(Paths.get("C:\\Users\\Public\\IdeaProjects\\GatewayCommunication\\src\\gatewayIDFile"), scannerResult.getBytes());

                System.out.println("Gateway ID is: " + scannerResult);
        }

        } catch (IOException e) {
            e.printStackTrace();
        }

/*
        System.out.println("Waiting to receive something...");
        Scanner in = new Scanner(System.in);
        scannerResult = in.next();
        System.out.println("Received: " + in.next());
*/
        int loop = 1;
        while (loop < 2) {
            call(scannerResult);
            //scannerResult = "9"; //Should be the variable for the gateway id
            //diagTest = "d1"; //Here for testing

            if (diagTest != "") {
                System.out.println("Diagnostic " + diagTest + " requested.");
                DiagnosticStarter.main(args);
            }

            if (scheduledTest != "") {
                DiagnosticTimer.main(args);
            }
            diagTest = "";
            scheduledTest = "";
            TimeUnit.SECONDS.sleep(10); //Normally 60 seconds
        }

    }

    public static String call(String heartbeat) throws IOException, JSONException {
        URL urlConn = new URL("https://team11.dev.softwareengineeringii.com/api/CiC/heartbeat?gwToken=" + URLEncoder.encode(heartbeat, "UTF-8"));
        HttpURLConnection httpCon = (HttpURLConnection) urlConn.openConnection();
        httpCon.setRequestMethod("POST");
        httpCon.setRequestProperty("gwToken", heartbeat);


        httpCon.setDoOutput(true);

        OutputStream os = httpCon.getOutputStream();
        os.flush();
        os.close();

        StringBuilder response = new StringBuilder();
        Scanner httpResponseScanner = null;
        try {
            httpResponseScanner = new Scanner(httpCon.getInputStream());
        } catch (IOException e) {
            InputStream errResponse = httpCon.getErrorStream();
            if (errResponse != null) {
                httpResponseScanner = new Scanner(errResponse);
            }
        }

        if (httpResponseScanner != null) {
            while (httpResponseScanner.hasNextLine()) {
                response.append(httpResponseScanner.nextLine());
            }
            httpResponseScanner.close();
        } else {
            response.append("Unable to communicate with the server");
        }
        System.out.println("Heartbeat: " + response.toString());

        JSONObject myResponse = new JSONObject(response.toString());

        try {
            scheduledTest = myResponse.getString("scheduled");

        }catch (JSONException e){
            //System.out.println("No diagnostic requested");
            System.out.println("---------------------------");
        }

        try {
            diagTest = myResponse.getString("diagnostic");
            System.out.println("Diagnostic- " + myResponse.getString("diagnostic"));


        }catch (JSONException e){
            //System.out.println("No diagnostic requested");
            System.out.println("---------------------------");
            return response.toString();
        }
        return response.toString();
    }

}