import org.json.JSONException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void mainTest1() {

    }

    @Test
    public void callTest1() throws IOException, JSONException {
        String testString = "d1";
        String errorMessage = "Expected <" + testString + "> to not be empty";
        String response = Main.call(testString);
        assertFalse(errorMessage, response.equals(""));

    }
}