import org.json.JSONException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

// Java code for thread creation by implementing
// the Runnable Interface 
class MultithreadingTest implements Runnable
{
    public void run()
    {
        String diagProgram = DiagnosticStarter.diagProgram;

        //Change these paths!
        String pathToProgram = "C:/Users/Public/IdeaProjects/GatewayCommunication/src/" + diagProgram + ".py";
        String pathToPython = "python";

        try
        {
            //Thread object = new Thread(new MultithreadingTest());
            //object.start();
            Process p = Runtime.getRuntime().exec(pathToPython + " " + pathToProgram);

            BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder stdoutString = new StringBuilder();
            String s;

            bufferedReaderToStringBuilderMultithread(stdOut, stdoutString);
            System.out.println(Thread.currentThread() + ":" + stdoutString);
        } catch (java.io.IOException e) {
            System.out.println("An error occured running the diagnostic program!");
        }
    }

    public static void bufferedReaderToStringBuilderMultithread(BufferedReader stdOut, StringBuilder stdoutString) {

        String s;

        try
        {
            while ((s = stdOut.readLine()) != null)
            {
                stdoutString.append(s);
                stdoutString.append("\n");
            }

            diagnosticCallMultithread(s);
        }
        catch (IOException | JSONException e)
        {
            e.printStackTrace();

            System.err.println("Error!");
        }

    }

    public static String diagnosticCallMultithread(String diagnostic) throws IOException, JSONException {
        URL urlConn = new URL("https://team11.dev.softwareengineeringii.com/api/CiC/diagnostics");
        HttpURLConnection httpCon = (HttpURLConnection) urlConn.openConnection();
        httpCon.setRequestMethod("POST");
        httpCon.setRequestProperty("gwToken", Main.scannerResult);
        httpCon.setRequestProperty("diag", diagnostic);

        httpCon.setDoOutput(true);

        OutputStream os = httpCon.getOutputStream();
        os.flush();
        os.close();

        StringBuilder response = new StringBuilder();
        Scanner httpResponseScanner = null;
        try {
            httpResponseScanner = new Scanner(httpCon.getInputStream());
        } catch (IOException e) {
            InputStream errResponse = httpCon.getErrorStream();
            if (errResponse != null) {
                httpResponseScanner = new Scanner(errResponse);
            }
        }

        if (httpResponseScanner != null) {
            while (httpResponseScanner.hasNextLine()) {
                response.append(httpResponseScanner.nextLine());
            }
            httpResponseScanner.close();
        } else {
            response.append("Unable to communicate with the server");
        }

        System.out.println(response.toString());
        return response.toString();
    }

} 